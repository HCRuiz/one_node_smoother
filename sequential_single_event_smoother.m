%% Sequential single event smoother
% Loads the data given by single event BOLD time series and applies APIS to
% each of them sequentially

clear all
addpath  /vol/snn4/hruiz/APIS_annealing/General_functions %Z://hruiz//APIS_annealing//General_functions%
addpath /vol/snn3/hruiz/fMRI_inference/Data/SUBJECT1 %Y://hruiz//fMRI_inference//Data//SUBJECT1%
addpath /vol/snn3/hruiz/fMRI_inference/Code %Y://hruiz//fMRI_inference//Code%

%% Change file id  'BOLD_TS_*' where * can be M,V,A for Motor, Auditory or Visual
disp('LOADING DATA...')
file_id = 'BOLD_TS_M';
data_file = strcat(file_id,'.mat');
load(data_file)
load('SingleEvent_TS_all_ROIs.mat', 'event_times_singleTS')

dir_id = strcat('SUBJECT1/',dir_id)
mkdir(dir_id)
cd(dir_id)
pwd
%% CHANGE HERE also std_BOLD_*
% [number_of_TS , trials] = size(std_BOLD_M,2);
number_of_TS = size(std_BOLD_M,1);
fprintf('Total number of jobs: %i \n', number_of_TS)

time_maxEZ_SD = zeros(1,number_of_TS);
Expected_time_maxZ_SD = zeros(1,number_of_TS);
time_to_estimate = zeros(1,number_of_TS);

trial = 1
subdir = sprintf('trial%i/',trial)
mkdir(subdir)
cd(subdir)
for ts = 1:number_of_TS
    tic
    subdir = sprintf('ts%i',ts)
    mkdir(subdir) % This is the subdirectory where the fMRI_smoother function saves the data
    ROI_BOLD = BOLD_M(ts,:,trial);
    std_ROI_BOLD = std_BOLD_M(ts,trial);
    [mSD, varSD, BOLD, stdBOLD, time_maxEZ,Expected_time_maxZ] = fMRI_smoother( ROI_BOLD, std_ROI_BOLD, tobs, subdir );
    time_maxEZ_SD(ts) =  time_maxEZ;
    Expected_time_maxZ_SD(ts) =  Expected_time_maxZ;
    time_to_estimate(ts)  = toc
end

dir_id = sprintf('trial%i',trial)
dir_id = strcat(dir_id,'/',dir_id);
cd(dir_id)
save('time_peak_Z','time_maxEZ_SD','Expected_time_maxZ_SD','event_times_singleTS')
save('time_to_estimate','time_to_estimate')

cd('../../../')
disp(pwd)
disp('DONE!')

% plotsAPIS_DCM