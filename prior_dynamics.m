clear all
% addpath Z:/hruiz/APIS_annealing/General_functions
sys='DCM'
diff_case = sys;
 plots = 1;

% set(0,'defaulttextfontsize',18);
% set(0,'defaultaxesfontsize',18);
% set(0,'defaultfigurecolor',[1 1 1]);
%% Flags
%Choose type of inizialisation
pri='wide'
%Cases
noisy=1 % 1 = true and all variables are noisy; 0 = false, then only neuronal noise considered. 
%% GENERATE DATA
N = 5000
%PARAMETERS
%Integration period
Time=3; %Time horizon in seconds
T=Time*100; % Number of Integration steps
h=Time/T; %integration step
taxis=h*(0:T-1);
%Input: Box car function
t_on=400;
t_off=415;
% Neuronal Model
A=-1;
B=0;
C=0;

% Hemodynamic system: parameters for s 
epsilon=0.8;
tau_s=1.54;
tau_f=2.44;

%Balloon model: v and q
tau_0=1.02;
alpha=0.32;
alpha=1/alpha;
E_0=0.4;

%BOLD Signal
y=zeros(N,T);
V_0=0.018;
k1=7*E_0;
k2=2;
k3=2*E_0-0.2;

%% Initializations
% Initialize state X = [z;s;f;v;q]
X = zeros(N,5,T); 
%Initial Condition
switch pri
    case 'wide'
        sigma_z0 = 0.106;
        sigma_s0 = 0.122;
        sigma_f0 = 0.25;
        sigma_v0 = 0.077;
        sigma_q0 = 0.092;
    case 'fixed'
        pos='fixed'
        sigma_z0 = 0;
        sigma_s0 = 0;
        sigma_f0 = 0;
        sigma_v0 = 0;
        sigma_q0 = 0;
end

 mean_prior=repmat([0,0,1,1,1],N,1);
 std_prior= repmat([ sigma_z0, sigma_s0, sigma_f0, sigma_v0, sigma_q0],N,1);

X(:,:,1) = mean_prior + std_prior.*randn(N,5);

sigY = 0.004;
y(:,1) = V_0*(k1*(1-X(:,5,1)) + k2*(1-X(:,5,1)./X(:,4,1)) + k3*(1-X(:,4,1))) + sigY*randn;

%Noise
dW=sqrt(h)*randn(N,5,T);

switch noisy
    case 1
        sigZ = 0.15;
        sigS=0.05;
        sigF=0.15;
        sigV=0.01;
        sigQ=0.01;
    case 0
         sigZ = 0;
        sigS=0;
        sigF=0;
        sigV=0;
        sigQ=0;
end

param_Z = [t_on,t_off,A,B,C];
param_HD = [epsilon,tau_s,tau_f];
param_Balloon = [tau_0,alpha,E_0];
param = [param_Z,param_HD,param_Balloon];
SIGMA = repmat([sigZ;sigS;sigF;sigV;sigQ]',N,1);

%% Integration
for t=1:T-1
    %Neural activity
    X(:,:,t+1) = X(:,:,t) + Drift(t,X(:,:,t),param,sys)*h + SIGMA.*diffusion(X(:,:,t),sys).*dW(:,:,t) ;
    if sum(X(:,3,t+1)<0) || sum(X(:,4,t+1)<0) 
        X(:,3,t+1) = abs(X(:,3,t+1));
        X(:,4,t+1) = abs(X(:,4,t+1));
%         error('ABORT! Unphysical process!!')
    end
    %BOLD signal
    y(:,t+1) = V_0*(k1*(1-X(:,5,t+1)) + k2*(1-X(:,5,t+1)./X(:,4,t+1)) + k3*(1-X(:,4,t+1))) + sigY*randn;
end

obs=1:45:T;
Yx=y(:,obs) ;
dim_obs=size(Yx,1);

 meanSD_prior = squeeze(mean(X));
stdSD_prior =  squeeze(std(X));
figure; plot(stdSD_prior')

switch plots
    case 1
% Hidden Process and Data
        z = meanSD_prior(1,:);
        s = meanSD_prior(2,:);
        f = meanSD_prior(3,:);
        v = meanSD_prior(4,:);
        q = meanSD_prior(5,:);
        
        std_z = stdSD_prior(1,:);
        std_s = stdSD_prior(2,:);
        std_f = stdSD_prior(3,:);
        std_v = stdSD_prior(4,:);
        std_q = stdSD_prior(5,:);
        
        figure
        subplot(2,3,1)
        plot(taxis,z)
        hold on
         plot(taxis,z+std_z,':')
         plot(taxis,z-std_z,':')
        hold off
        ylabel('N. Activity z')
        subplot(2,3,2)
        plot(taxis,s)
        hold on
         plot(taxis,s+std_s,':')
         plot(taxis,s-std_s,':')
        hold off
        ylabel('Vasodilatory Signal s')
        subplot(2,3,3)
        plot(taxis,f)
                hold on
         plot(taxis,f+std_f,':')
         plot(taxis,f-std_f,':')
        hold off
        ylabel('Blood Inflow f')
        subplot(2,3,4)
        plot(taxis,v)
                hold on
         plot(taxis,v+std_v,':')
         plot(taxis,v-std_v,':')
        hold off
        ylabel('Normalized Blood Volume v')
        subplot(2,3,5)
        plot(taxis,q)
                hold on
         plot(taxis,q+std_q,':')
         plot(taxis,q-std_q,':')
        hold off
        ylabel('Deoxygenated Blood (dHb) q')
        subplot(2,3,6)
        plot(taxis,mean(y))
        hold on
%         plot(obs*h,Yx,'dr','MarkerFace','r')
        hold off
        ylabel('BOLD Signal y')
    case 0
        disp('Plots disabled')
end