% Estimate the smoothing distribution of a single node of a DCM where the
% hidden variables (z,s,f,v,q) are all noisy

clear all
addpath /vol/snn4/hruiz/APIS_annealing/General_functions
addpath /vol/snn3/hruiz/fMRI_inference/Data

set(0,'defaulttextfontsize',18);
set(0,'defaultaxesfontsize',18);
set(0,'defaultfigurecolor',[1 1 1]);
plots = 0;

%% GENERATE DATA
sys='DCM';
diff_case = sys;

% disp('Generating & Saving Data...')
% data_generator
% save('Data_statedepNoise')

disp('LOADING DATA...')
load('Data_statedepNoise')

%% SMOOTHER
%Param of DCM
D=5;
% param = [ t_on ,t_off, A, B, C, epsilon, tau_s, tau_f, tau_0, alpha, E_0 ];
param(5)=0;
disp('Input switched off after data generation!')
noise_factor = 6; % with value <=3 doesn't learn input (?)
sigZ = noise_factor*sigZ;
fprintf('Neuronal noise increased by %1.2f !',noise_factor)
dyn_var = [sigZ^2,sigS^2,sigF^2,sigV^2,sigQ^2]; %SIGMA'.^2; %

% Param BOLD
signal_type = 'BOLD';
param_signal = [V_0,k1,k2,k3];

% Param Observ. model
obs_model = 'Gauss';
yvar=sigY^2;
param_obs = yvar;
        
%Parameters of APIS
pos='adapt'
N=50000;
iters=750;
ESSstop=1.1;
learning_rate=0.005;
gamma=200/N;
beta=1.12;
extra = 'no'; %'cost' enables calculation of mean cost and its variance
param_disp = fprintf('N = %i, eta = %1.3f, iters = %i \n',N,learning_rate,iters);

save('ParametersAPIS_InpOFF_statedepNoise','D','param','dyn_var','noise_factor','signal_type','param_signal','obs_model','yvar','param_obs','pri','pos','N','iters','ESSstop','learning_rate','gamma','beta','extra')

APIS_annealing

BOLD =wend'*squeeze(Obs_Signal);
stdBOLD = sqrt(var(squeeze(Obs_Signal),wend));

save('Data_plots_statedepNoise','PSESS','ESS_raw','taxis','mSD','varSD','y','BOLD','stdBOLD','obs','h','Yx','b', 'A')

save('Results_InpOFF_statedepNoise')

switch plots
    case 1
        %SMOOTHING ESTIMATE
        figure;
        for dd=1:D
            subplot(2,3,dd)
            hold on
            plot(taxis,mSD(:,dd),'r')
            plot(taxis,mSD(:,dd)+sqrt(varSD(:,dd)),':r')
            plot(taxis,mSD(:,dd)-sqrt(varSD(:,dd)),':r')
            if dd==1
                plot(taxis,I,'k')
            end
            hold off
        end
        subplot(2,3,6)
        
        hold on
        BOLD =wend'*squeeze(Obs_Signal);
        stdBOLD = sqrt(var(squeeze(Obs_Signal),wend));
        plot(taxis,BOLD,'r')
        plot(taxis,BOLD+stdBOLD,':r')
        plot(taxis,BOLD-stdBOLD,':r')
        plot(obs*h,Yx,'dr','MarkerFace','r')
        hold off
        
        figure
        for dd=1:D
            subplot(2,3,dd)
            plot(squeeze(var(PS(:,dd,:))),'b')
            hold on
            plot(varSD(:,dd),'r')
            hold off
        end
        subplot(2,3,6)
        plot(var(squeeze(Obs_Signal),wend),'b')
        hold on
        plot(stdBOLD.^2,'r')
        hold off
        
        % OPEN LOOP CONTROLLER
        figure
        subplot(1,2,1)
        plot(taxis,b)
        legend('b_1','b_2','b_3','b_4','b_5')
        STR=sprintf('Open loop controller');
        title(STR)
        % Diagonal elements of the feedback matrix
        subplot(1,2,2)
        hold on
        plot(taxis,squeeze(A(1,1,:)),'b')
        plot(taxis,squeeze(A(2,2,:)),'g')
        plot(taxis,squeeze(A(3,3,:)),'k')
        plot(taxis,squeeze(A(4,4,:)),'c')
        plot(taxis,squeeze(A(5,5,:)),'m')
        legend('A_{1,1}','A_{2,2}','A_{3,3}','A_{4,4}','A_{5,5}')
        plot(h*(obs-1),0,'.r')
        STR=sprintf('Feedback controller; N=%i',N);
        title(STR)
        hold off
        % The feedback matrix
        figure
        for dd=1:D
            for gg=1:D
                subplot(5,5,(dd-1)*D+gg)
                plot(taxis,squeeze(A(dd,gg,:)))
                axis([0 taxis(end) -inf inf])
                hold on
                plot(h*(obs-1),0,'dr','MarkerFace','r')
                hold off
            end
        end
        % ESS
        STR=sprintf('ESS over iterations, Initialization: %s, N=%i',pri,N);
        figure;
        plot(PSESS,'-b')
        hold on;
        plot(ESS_raw,'r')
        hold off
        label('ESS','raw ESS')
        title(STR)
        
        %EXTRAS
        switch extra
            case 'cost'
                STR=sprintf('E_{u}[S_{0}] and <S_{0}> vs iters, Initialization: %s, N=%i, smoother=%1.2f',pri,N,a);
                figure;
                plot(mS0_iter,'b')
                hold on
                plot(wmS0_iter,'k')
                legend('E_{u}[S_{0,u}]','<S_{0,u}>')
                plot(1:iters,mS0_iter+stdS0_iter,'-.r',1:iters,mS0_iter-stdS0_iter,'-.r')
                plot(1:iters,wmS0_iter+wstdS0_iter,':k',1:iters,wmS0_iter-wstdS0_iter,':k')
                title(STR)
                hold off
        end
        
    case 0
        disp('Plots disabled')
end